import React, { useEffect, useRef } from 'react';

interface Props {
  onClick?: (event: MouseEvent) => void;
  children: JSX.Element;
}

export default function ClickAway(props: Props) {
  const ele = useRef<HTMLDivElement>(null);
  const clickHandler = (event: MouseEvent) => {
    if (ele.current && ele.current.contains(event.target as Node)) return;
    props.onClick && props.onClick(event);
  };

  useEffect(() => {
    document.addEventListener('click', clickHandler);

    return () => {
      document.removeEventListener('click', clickHandler);
    };
  }, []);

  return <span ref={ele}>{props.children}</span>;
}
