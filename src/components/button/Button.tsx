import React from 'react';
import { Button, ButtonProps } from './Button.style';

interface Props extends ButtonProps {
  children: React.ReactChild;
}

export default function(props: Props) {
  return <Button {...props}>{props.children}</Button>;
}
