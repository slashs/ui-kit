import styled, { css } from 'styled-components';
import { colors } from '../color';

export interface ButtonProps {
  primary?: boolean;
  secondary?: boolean;
}

export const Button = styled.button`
  cursor: pointer;
  font-family: source-han-sans-traditional, sans-serif;
  font-weight: 300;
  background-color: transparent;
  font-size: 14px;
  line-height: 38px;
  border: 1px solid ${colors.gray3};
  color: ${colors.gray7};
  padding: 0 30px;
  transition: 0.2s ease-in-out;
  border-radius: 3px;

  &:hover,
  &:active,
  &:focus {
    border: 1px solid ${colors.gray7};
    outline: none;
  }

  ${(props: ButtonProps) =>
    props.primary &&
    css`
      background: ${colors.blue5};
      color: white;
      border: 1px solid ${colors.blue5};

      &:hover,
      &:active,
      &:focus {
        border: 1px solid ${colors.blue6};
        background: ${colors.blue6};
      }
    `}

  ${(props: ButtonProps) =>
    props.secondary &&
    css`
      background: ${colors.gray8};
      color: white;
      border: 1px solid ${colors.gray8};

      &:hover,
      &:active,
      &:focus {
        border: 1px solid ${colors.gray9};
        background: ${colors.gray9};
      }
    `}
`;
