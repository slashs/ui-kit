import styled from 'styled-components';

export const Dropdown = styled.div`
  font-family: source-han-sans-traditional, sans-serif;
  font-weight: 300;
  display: inline-block;
  position: relative;
`;

export const Menu = styled.div`
  position: absolute;
  min-width: 200px;
  padding: 20px;
  background: white;
  border-radius: 3px;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
`;
