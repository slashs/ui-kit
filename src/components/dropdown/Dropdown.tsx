import React, { useEffect, useRef, useState } from 'react';
import ClickAway from '../clickaway';

import { Dropdown, Menu } from './Dropdown.style';

interface Props {
  pos?: 'bottom-right';
  overlay?: React.ReactNode;
  children?: React.ReactChild;
}

export default function(props: Props) {
  const anchorEl = useRef<HTMLDivElement>(null);
  const menuEl = useRef<HTMLDivElement>(null);
  const [open, setOpen] = useState(false);
  useEffect(() => {
    if (anchorEl.current && menuEl.current) {
      const anchorHeight = anchorEl.current.offsetHeight;
      menuEl.current.style.top = `${anchorHeight + 10}px`;
      if (props.pos === 'bottom-right') {
        menuEl.current.style.right = '0';
      }
    }
  }, [props.pos, open]);

  const closeOverlay = () => setOpen(false);
  const openOverlay = () => setOpen(true);
  return (
    <ClickAway onClick={closeOverlay}>
      <Dropdown>
        <div ref={anchorEl} onClick={openOverlay}>
          {props.children}
        </div>
        {open && <Menu ref={menuEl}>{props.overlay}</Menu>}
      </Dropdown>
    </ClickAway>
  );
}
