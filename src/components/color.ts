export const colors = {
  blue1: '#e6e8f3',
  blue2: '#bfc6e1',
  blue3: '#96a2cc',
  blue4: '#6d7eb8',
  blue5: '#4d63aa',
  blue6: '#2b499c',
  blue7: '#254293',
  blue8: '#1b3888',
  blue9: '#112f7c',
  blue10: '#001e66',

  gray1: '#f7f7f7',
  gray2: '#eeeeee',
  gray3: '#e2e2e2',
  gray4: '#cfcfcf',
  gray5: '#aaaaaa',
  gray6: '#898989',
  gray7: '#626262',
  gray8: '#4f4f4f',
  gray9: '#313131',
  gray10: '#111111',

  red1: '#ffebed',
  red2: '#ffcccf',
  red3: '#f89893',
  red4: '#f07069',
  red5: '#f94e40',
  red6: '#fd3c1e',
  red7: '#ef3120',
  red8: '#dd241b',
  red9: '#d11a13',
  red10: '#c20000',

  green1: '#e6f6e4',
  green2: '#c4e7be',
  green3: '#9cd893',
  green4: '#71c966',
  green5: '#4dbe41',
  green6: '#1fb20f',
  green7: '#0ea300',
  green8: '#009100',
  green9: '#008000',
  green10: '#006100'
};
