import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

import { StyledFrame, StyledModal } from './Modal.style';
import posed, { PoseGroup } from 'react-pose';

interface Props {
  onClose?: () => void;
  open: boolean;
  children?: React.ReactChild;
}

export default function(props: Props) {
  const onClickAway = () => {
    if (props.open) {
      props.onClose && props.onClose();
    }
  };

  const [node] = useState(document.createElement('div'));

  useEffect(() => {
    document.body.appendChild(node);
    return () => {
      node && document.body.removeChild(node);
    };
  }, []);

  const model = (
    <PoseGroup>
      {props.open && (
        <StyledFrame key="modal_frame" onClick={onClickAway}>
          <StyledModal>{props.children}</StyledModal>
        </StyledFrame>
      )}
    </PoseGroup>
  );

  return ReactDOM.createPortal(model, node);
}
