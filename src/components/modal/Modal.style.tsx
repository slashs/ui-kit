import styled from 'styled-components';

import posed, { PoseGroup } from 'react-pose';
const Frame = posed.div({
  enter: {
    opacity: 1,
    transition: { duration: 150, ease: 'linear' }
  },
  exit: {
    opacity: 0,
    transition: { duration: 150, ease: 'linear' }
  }
});

export const StyledFrame = styled(Frame)`
  font-family: source-han-sans-traditional, sans-serif;
  font-weight: 300;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
`;

export const StyledModal = styled.div`
  position: fixed;
  background: white;
  width: 500px;
  height: auto;
  padding: 20px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 3px;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
`;
