import React, { useState } from "react";
import styled from "styled-components";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import { Welcome } from "@storybook/react/demo";
import Button from "../components/button";
import Dropdown from "../components/dropdown";
import Modal from "../components/modal";
import { Colors } from "./color-demo";

const Container = styled.div`
  & > * {
    margin: 5px;
  }
`;

const ModalFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;

  & > * {
    margin-left: 5px;
  }
`;

storiesOf("Color", module).add("colors", () => <Colors />);

storiesOf("Components", module)
  .add("button", () => (
    <Container>
      <div>
        <Button>Default</Button>
      </div>
      <div>
        <Button primary>Primary</Button>
      </div>
      <div>
        <Button secondary>Secondary</Button>
      </div>
    </Container>
  ))
  .add("dropdown", () => {
    const menu = (
      <div>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt.
      </div>
    );
    return (
      <Container>
        <Dropdown overlay={menu}>
          <Button>Dropdown</Button>
        </Dropdown>
        <Dropdown overlay={menu} pos="bottom-right">
          <Button>Bottom Right</Button>
        </Dropdown>
      </Container>
    );
  })
  .add("modal", () => {
    const ModalDemo = () => {
      const [open, setOpen] = useState(false);
      return (
        <Container>
          <Button onClick={() => setOpen(true)}>Open Modal</Button>
          <Modal open={open} onClose={() => setOpen(false)}>
            <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt.
            </div>
            <ModalFooter>
              <Button onClick={() => setOpen(false)}>Cancel</Button>
              <Button onClick={() => setOpen(false)} primary>
                Accept
              </Button>
            </ModalFooter>
          </Modal>
        </Container>
      );
    };
    return <ModalDemo />;
  });
