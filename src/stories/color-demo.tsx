import React from 'react';
import styled from 'styled-components';
import { colors } from '../components/color';

const Name = styled.div`
  width: 75px;
`;

const Value = styled.div`
  width: 100px;
`;

const ColorBlock = styled.div`
  width: 20px;
  height: 20px;
  background: ${(props: { value: string }) => props.value};
`;

const Container = styled.div`
  font-family: source-han-sans-traditional, sans-serif;
  font-weight: 300;
  font: 14px;
  display: flex;
  align-items: center;
  width: 300px;

  & > * {
    margin: 2px 5px;
  }
`;

interface Props {
  name: string;
  value: string;
}

const Color = ({ name, value }: Props) => (
  <Container>
    <ColorBlock value={value} />
    <Name>{name}:</Name>
    <Value>{value}</Value>
  </Container>
);

export const Colors = () => {
  return Object.entries(colors).map(([k, v]) => <Color name={k} value={v} />);
};
